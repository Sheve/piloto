<?php

require_once 'dbManager.php';
require_once 'Mobile_Detect.php';

class DatosCliente {

    public function LoadDatos() {
        $info['ip'] = file_get_contents('https://api.ipify.org');
        $date = new DateTime();
        $info['fecha'] = $date->format('Y-m-d H:i:s');
        $browser = array("IE", "OPERA", "MOZILLA", "NETSCAPE", "FIREFOX", "SAFARI", "CHROME");
        $os = array("WIN", "MAC", "LINUX");
        $info["agente"] = $_SERVER['HTTP_USER_AGENT'];
# definimos unos valores por defecto para el navegador y el sistema operativo
        $info['browser'] = "OTHER";
        $info['os'] = "OTHER";
# buscamos el navegador con su sistema operativo
        foreach ($browser as $parent) {
            $s = strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $parent);
            $f = $s + strlen($parent);
            $version = substr($_SERVER['HTTP_USER_AGENT'], $f, 15);
            $version = preg_replace('/[^0-9,.]/', '', $version);
            if ($s) {
                $info['browser'] = $parent;
                $info['version'] = $version;
            }
        }
# obtenemos el sistema operativo
        foreach ($os as $val) {
            if (strpos(strtoupper($_SERVER['HTTP_USER_AGENT']), $val) !== false)
                $info['os'] = $val;
        }
        $detect = new Mobile_Detect();
        // Basic detection.
        $info['isMobile'] = $detect->isMobile();
        $info['movil'] = $detect->getMatchingRegex();
        

# insertamos los valores en la base de datos
        $db = new DbManager(DbManager::SERVER_DESARROLLO);
        $db->saveClient($info);
        
        return $info;
    }

}

?>