<?php

class DbManager {

    const SERVER_DESARROLLO = 'desarrollo';
    const SERVER_PRODUCCION = 'produccion';

    var $tipoServer;

    function __construct($tipoServer) {
        $this->tipoServer = $tipoServer;
    }

    private function connect() {
        if (!isset($mysqli)) {
            if ($this->tipoServer == self::SERVER_PRODUCCION) {
                $mysqli = new mysqli("127.0.0.1", "msj_usr", "um+e6C8=1PBV", "msj_pruebapiloto");
            }
            if($this->tipoServer == self::SERVER_DESARROLLO){
                $mysqli = new mysqli("127.0.0.1", "root", "", "pruebapiloto");
            }
        }
        if ($mysqli === false) {
            return false;
        }
        return $mysqli;
    }




    public function saveClient($info) {
        $conn = $this->connect();
        $isMobil = $info['isMobile'];
        $sentencia = $conn->prepare("INSERT INTO datocliente (ipCliente,os,navegador,versionNavegador,fecha,isMovil,movil)VALUES(?,?,?,?,?,?,?)");
        $sentencia->bind_param("sssssis", $info["ip"], $info["os"], $info["browser"], $info["version"], $info["fecha"], $isMobil, $info["movil"]);
        $valor = $sentencia->execute();
        $conn->close();
    }

}

?>